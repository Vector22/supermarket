HI AND WELLCOME TO MY PROJECT OF SUPERMARKET FOUND MANAGEMENT

# How to install

first clone the project source on bitbucket

git clone https://Vector22@bitbucket.org/Vector22/supermarket.git

after go to the root folder of the project



# initialize the development database

go to the django shell by typing : ./manage.py shell

import the fill_db modul by typing : import fill_db

fill the db by typing : fill_db.init_db()
after this line you must see "Warning database initialized"
if not, please read the previous section carefully

# play with datas

before playing, you must import the required class to do this in django shell

from management.models import Article, Cart, Ticket

and the fun can finaly begin

# create a cart

cart = Cart()

# add article to a cart
a1 = Article.objects.get(id=1)

# start a promotion for an article
a1.start_promotion(promotion_type) ; where promotion_type must be 'FP' or 'BT'
('FP' for 50% of reduction on the article, and 'BT' is buy two article an get one bonus)

# stop promotion for an article
a1.stop_promotion()

# add articles to the cart
a2 = Article.objects.get(id=2)
a2.start_promotion('BT') [article in promotion]

cart.add_article(a1, 3)
cart.add_article(a2, 5)

# Save cart
as the cart must be (i think), dynamicaly created, you must save them if you want
preserve the data. In our case, they are stored in a sqlite3 database

cart.save()

# create a ticket
ticket = Ticket()

# specify the cart of the ticket
ticket.cart = cart [our previous cart created]

# print the details of the ticket [articles contained in the cart]
ticket.show_details()

# Update article stock
a1.update_stock(quantity, false) [please go to see in the Cart model for more details]


THAT WAS IT !

This project is under developpment, please many changes are comming soon

For any bugs or usefull inforation, you can mail me on ykouadioulrich@gmail.com