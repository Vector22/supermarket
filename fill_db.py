from management.models import Article
from data import datas


# Insert the datas in the db

def init_db():
    for article in datas:
        a = Article(name=article['name'], description=article['description'],
                    price=article['price'], stock=article['stock'])
        a.save()

    print('\nWarning: Database initialized !!!\n\n')
