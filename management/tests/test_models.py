from django.test import TestCase

from management.models import Article, Cart, Ticket


import json


class ArticleTest(TestCase):

    # initialize some test variables
    @classmethod
    def setUpTestData(cls):
        Article.objects.create(name='an article', price=500, stock=100)
        Article.objects.create(name='another article', price=345, stock=30)

    def setUp(self):
        self.article1 = Article.objects.get(id=1)
        self.article1.start_promotion('FP')

    def test_articles_database(self):
        """ Test if the articles are correctly saved in the
        database """

        self.assertEqual(Article.objects.count(), 2)

    def test_update_stock_in_sell_case(self):
        """ Test the update_stock method for the sell case
        [or when we remove some articles from the stock] """

        # the case where we sell 20 article1
        self.article1.update_stock(20)
        self.article1.refresh_from_db()

        self.assertEqual(self.article1.stock, 80)  # 100-20=80

    def test_update_stock_in_supply_case(self):
        """ Test the update_stock method for the supply case
        [or when we add some articles to the stock] """

        # we add 50 articles on the article1 stock
        self.article1.update_stock(50, False)
        self.article1.refresh_from_db()

        self.assertEqual(self.article1.stock, 150)  # 100+50=150

    def test_update_stock_with_negative_quantity(self):
        """ Test the update_stock method for the both cases
        with a negative quantity provided as argument """

        # the function return the current stock
        # and we see that it's let unchanged
        stock = self.article1.update_stock(-10)
        self.assertEqual(stock, 100)

        stock = self.article1.update_stock(-10, False)
        self.assertEqual(stock, 100)

    def test_update_stock_with_more_than_available(self):
        """ Test the update_stock method for the sell case
        when the quantity is greater than the available stock"""

        # we try to sell more than we have in stock[100]
        stock = self.article1.update_stock(200)
        self.assertEqual(stock, 100)

    def test_start_promotion(self):
        article2 = Article.objects.get(id=2)
        article2.start_promotion('BT')
        article2.refresh_from_db()

        self.assertTrue(article2.in_promotion)
        self.assertEqual(article2.promotion_type, 'BT')

        # try to start promotion on an article without stop
        # the previous promotion before
        article2.start_promotion('FP')
        # we se that the promotion type dont change
        self.assertEqual(article2.promotion_type, 'BT')

    def test_stop_promotion(self):
        """ Test if we can stop a promotion
        of an article """

        # article1 is already in promotion
        self.article1.stop_promotion()
        self.assertFalse(self.article1.in_promotion)


class CartTest(TestCase):

    # initialize some test variables
    @classmethod
    def setUpTestData(cls):
        Article.objects.create(name='an article', price=500, stock=100)
        Article.objects.create(name='another article', price=345, stock=30)
        Article.objects.create(name='a fake article', price=180, stock=15)

    def setUp(self):
        self.article1 = Article.objects.get(id=1)
        self.article1.start_promotion('FP')

        self.article2 = Article.objects.get(id=2)
        self.article3 = Article.objects.get(id=3)

    def test_add_article(self):
        # Need to add the price as a decimal string
        # if not, json.dumps method don't work
        expected_content = {0: {"name": 'an article', "price": "500.00",
                                "in_promotion": True, "promotion_type": 'FP',
                                "quantity": 3},
                            }
        expected_content2 = {0: {"name": 'an article', "price": "500.00",
                                 "in_promotion": True, "promotion_type": 'FP',
                                 "quantity": 3},
                             1: {"name": 'another article', "price": "345.00",
                                 "in_promotion": False, "promotion_type": '',
                                 "quantity": 1}, }

        cart1 = Cart()
        cart1.add_article(self.article1, 3)
        self.assertEqual(cart1.articles, json.dumps(expected_content))

        # add another article
        cart1.add_article(self.article2, 1)
        self.assertEqual(cart1.articles, json.dumps(expected_content2))

    def test_del_article(self):
        expected_content = {0: {"name": 'an article', "price": "500.00",
                                "in_promotion": True, "promotion_type": 'FP',
                                "quantity": 3},
                            }
        cart1 = Cart()
        cart1.add_article(self.article1, 3)
        self.assertEqual(cart1.articles, json.dumps(expected_content))

        # delete the article, then the cart must be empty
        cart1.del_article("an article")
        self.assertEqual(cart1.articles, '{}')

    def test_has_article(self):
        cart1 = Cart()
        cart1.add_article(self.article1, 1)
        cart1.add_article(self.article2, 2)
        # present in the cart
        self.assertTrue(cart1.has_article("an article"))
        self.assertTrue(cart1.has_article("another article"))
        # not present in the cart
        self.assertFalse(cart1.has_article("fake article"))

    def test_show_articles(self):
        expected_content = {0: {"name": 'an article', "price": "500.00",
                                "in_promotion": True, "promotion_type": 'FP',
                                "quantity": 3},
                            }
        cart1 = Cart()
        cart1.add_article(self.article1, 1)
        # compare the json content of the articles field
        self.assertTrue(json.loads(cart1.articles), expected_content)

    def test_show_articles_name(self):
        articles_name = ["an article", ]
        articles_name2 = ["an article", "another article"]

        cart1 = Cart()
        cart1.add_article(self.article1, 3)
        # add one article
        self.assertEqual(cart1.show_articles_name(), articles_name)
        # add another articcle => two names in the list
        cart1.add_article(self.article2, 1)
        self.assertEqual(cart1.show_articles_name(), articles_name2)


class TicketTest(TestCase):

    # initialize some test variables
    @classmethod
    def setUpTestData(cls):
        Article.objects.create(name='an article', price=500, stock=100)
        Article.objects.create(name='another article', price=345, stock=30)

        Article.objects.get(id=1).start_promotion('FP')

    def setUp(self):
        self.cart1 = Cart()
        self.cart1.add_article(Article.objects.get(id=1), 2)
        self.cart1.add_article(Article.objects.get(id=2), 3)

        self.ticket1 = Ticket()
        self.ticket1.cart = self.cart1

    def test_show_details(self):
        article1_details = "Article:an article, quantity:2, price:500.0(50% of reduction)"
        article2_details = "Article:another article, quantity:3, price:1035.0"
        total_price = "TOTAL PRICE : 1535.0"
        self.assertIn(article1_details, self.ticket1.show_details())
        self.assertIn(article2_details, self.ticket1.show_details())
        self.assertIn(total_price, self.ticket1.show_details())
