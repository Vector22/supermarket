from django.urls import path

from .views import HomeView, ArticleListView, ArticleDetailView, results
from .views import cart

urlpatterns = [
    path('home/', HomeView.as_view(), name='home'),
    path('results/', results, name='results'),
    path('articles/', ArticleListView.as_view(), name='article_list'),
    path('articles/<int:pk>/', ArticleDetailView.as_view(), name='article_detail'),
    path('cart/', cart, name='cart'),
]
