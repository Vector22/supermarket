from django.db import models
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone

from enum import Enum

import json


class PromotionType(Enum):
    """The differents types of article promotion"""
    FP = "Fifty percent of reduction"
    BT = "Buy Two and get one more"

    @classmethod
    def all(self):
        return [PromotionType.FP, PromotionType.BT]


class Article(models.Model):
    """Class who represent a supermarket article"""
    name = models.CharField(max_length=150)
    description = models.TextField(null=True)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    stock = models.PositiveIntegerField()
    in_promotion = models.BooleanField(default=False)
    promotion_type = models.CharField(
        max_length=3,
        null=True, blank=True
    )

    def __str__(self):
        return f"Name:{self.name}, Price:{self.price}, Stock:{self.stock}"

    def update_stock(self, quantity, remove=True):
        """Update the article stock after a customer order
        or a stock supply"""

        # The quantity to remove or add must be positive
        if quantity < 0:
            print("The quantity must be a positive integer")
            return self.stock  # Return the stock who remain unchanged

        if remove:  # we remove from the stock
            try:
                new_stock = self.stock - quantity
                if new_stock < 0:
                    raise ValueError("The quantity wanted is not available")
            except ValueError as e:
                print(e.__class__.__name__, e)
            else:
                self.stock = new_stock
        else:  # Obviously, it is a supply
            self.stock += quantity
        self.save()
        return self.stock  # We return the new stock in either case

    def start_promotion(self, promotion_type):
        """Put an article in promotion according to
        the promotion type"""

        if not promotion_type in ['BT', 'FP']:
            print("Bad promotion type entered")
            return False
        elif self.in_promotion:
            print("This article is already in promotion")
            return False
        else:
            if promotion_type is 'FP':
                self.in_promotion = True
                self.promotion_type = 'FP'
            else:  # the type is obviously PromotionType.BT
                self.in_promotion = True
                self.promotion_type = 'BT'
        self.save()
        return True

    def stop_promotion(self):
        """Stop the promotion of an article"""

        if not self.in_promotion:
            print("This article is not in promotion")
            return False
        else:
            self.in_promotion = False
            self.promotion_type = None
        self.save()
        return True


class Cart(models.Model):
    """Class who represent the cart of a customer"""

    # Use this to store articles in json string format
    articles = models.TextField(default='{}')
    size = models.PositiveSmallIntegerField(default=0)
    solved = models.BooleanField(default=False)
    make_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Number:{self.id}--{self.size} articles inside"

    def add_article(self, article, quantity):
        a = {}
        a['name'] = article.name
        a['price'] = article.price
        if article.in_promotion:
            a['in_promotion'] = True
            a['promotion_type'] = article.promotion_type
        else:
            a['in_promotion'] = False
            a['promotion_type'] = ''
        if type(quantity) is int and quantity > 0:
            a['quantity'] = quantity
        else:
            raise ValueError

        articles_dict = json.loads(self.articles)

        articles_dict[self.size] = a
        self.size += 1

        # Update the articles attribut
        self.articles = json.dumps(articles_dict, cls=DjangoJSONEncoder)

    def del_article(self, name):
        articles_dict = json.loads(self.articles)
        article_id = self.has_article(name)
        if article_id:
            del articles_dict[article_id]
            self.articles = json.dumps(articles_dict)
            return True
        return False

    def has_article(self, name):
        articles_dict = json.loads(self.articles)
        for id, article in articles_dict.items():
            if str(name) in article.values():
                return id
        return False

    def show_articles(self):
        print(json.loads(self.articles))

    def show_articles_name(self):
        articles_dict = json.loads(self.articles)
        return [article['name'] for article in articles_dict.values()]


class Ticket(models.Model):
    """The payment receipt"""
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, )

    def __str__(self):
        return f"cart_id: {self.cart.id}, date: {self.date}"

    # Override the save method to update stock after
    # a ticket is created (it means that the customer
    # has paid the articles)

    def save(self, *args, **kwargs):
        articles_dict = json.loads(self.cart.articles)
        for article in articles_dict.values():
            # retrieve the name and quantity of article ordered
            article_name = article['name']
            article_qty = article['quantity']
            # retrieve the article instance from the db
            article_object = Article.objects.get(name=article_name)
            # update the stock of this article
            article_object.update_stock(article_qty)
            article_object.save()
            # refresh the value in the db
            article_object.refresh_from_db()
        # Call the real save method of the Ticket class
        super(Ticket, self).save(*args, **kwargs)

    def show_details(self):
        """Show the ticket's details and give the total price"""
        articles_dict = json.loads(self.cart.articles)
        details = f"\n\t\tOrder details for Ticket number: {self.id}\n\n"
        article_details = ""
        ticket_price = float(0)

        for article in articles_dict.values():
            # breakpoint()
            article_name = str(article['name'])
            article_qty = int(article['quantity'])
            article_price = float(article['price'])
            article_bonus = 0

            # if article is in promotion
            if article['in_promotion'] is True:
                if str(article['promotion_type']) == 'FP':
                    # breakpoint()
                    article_price = float(article_price)/2
                    article_total_price = int(article_qty)*float(article_price)
                    ticket_price += article_total_price
                    article_details += """
                        Article:{}, quantity:{}, price:{}(50% of reduction)\n
                        """.format(article_name, article_qty,
                                   article_total_price)
                elif str(article['promotion_type']) == 'BT':
                    # breakpoint()
                    article_bonus = int(article_qty)//2
                    article_total_price = int(article_qty)*float(article_price)
                    article_qty += article_bonus
                    ticket_price += article_total_price
                    article_details += """
                        Article:{}, quantity:{}(+{} bonus), price:{}\n
                        """.format(article_name, article_qty,
                                   article_bonus, article_total_price)
                else:
                    pass
            # No promotion on article
            else:
                # breakpoint()
                article_total_price = int(article_qty)*float(article_price)
                ticket_price += float(article_total_price)
                article_details += """
                    Article:{}, quantity:{}, price:{}\n
                    """.format(article_name, article_qty, article_total_price)
        # breakpoint()
        details += article_details
        details += f"TOTAL PRICE : {ticket_price}\n"
        print(details)
        return(details)
