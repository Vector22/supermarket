from django.views.generic import TemplateView, ListView, DetailView
from django.shortcuts import render, redirect
from django.contrib import messages

from .models import Article, Cart  # Ticket


class HomeView(TemplateView):
    template_name = "home.html"


def results(request):
    """Return a list of searched article by the user"""

    if request.POST:
        # retrieve the world entered by the user
        world = str(request.POST.get('search', None))

        # looking for articles who are these worlds in their name
        articles = Article.objects.filter(name__contains=world)
        if articles.count():
            return render(request, 'management/results.html', locals())
        else:
            return render(request, 'management/articles_not_found.html',
                          locals())
    return render(request, 'management/articles.html')


class ArticleListView(ListView):
    model = Article
    context_object_name = "articles"
    template_name = "management/articles.html"
    paginate_by = 10
    queryset = Article.objects.all()  # Default is model.objects.all()


class ArticleDetailView(DetailView):
    model = Article
    context_object_name = "article"
    template_name = "management/articles_detail.html"


def cart(request):
    """This fuction perform the task to add an article
    to the cart"""

    # if we add some article to the cart
    if request.POST:
        # retrieve the data transmited by the client
        try:
            articleId = int(request.POST.get('articleId', None))
            quantity = int(request.POST.get('quantity', None))
        except ValueError as error:
            messages.add_message(request, messages.ERROR, error)
            return redirect('/management/articles/')

        if articleId and quantity:
            article = Article.objects.get(id=articleId)

            if article:

                # retrieve the last cart
                count = Cart.objects.count()
                latest_cart, cart = Cart.objects.get_or_create(id=count)

                if latest_cart:
                    if latest_cart.solved:
                        print("\n In latest_cart[solved] case \n")
                        # start with a new cart
                        cart.add_article(article, quantity)
                        cart.save()
                        cart.show_articles()
                    else:
                        print("\n In latest_cart[else]case \n")
                        # continue with the previous one
                        latest_cart.add_article(article, quantity)
                        latest_cart.save()
                        latest_cart.show_articles()
                # the latest_card object does not exist
                elif cart:
                    print("\n In cart case \n")
                    cart.add_article(article, quantity)
                    cart.save()
                    cart.show_articles()
                else:
                    messages.add_message(request, messages.ERROR, error)
                    return redirect('/management/articles/')

                messages.add_message(request, messages.SUCCESS,
                                     f"{article.name} has been successfully\
                                     added to the cart")
                return redirect('/management/articles/')

    return render(request, 'management/cart.html')
