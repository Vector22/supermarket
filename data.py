
datas = [
    # Asus
    {
        'name': 'Asus ZenFone Max M2 Dual SIM TD-LTE IN 64GB ZB632KL',
        'description': """
        2018 Dec, Smartphone, 76.28x158.41x7.7 mm, Android,
        Qualcomm Snapdragon 632 SDM632, 4.00 GiB RAM, 64.0 GB ROM,
        720x1520, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 13.0 MP camera,
        1.9 MP aux. camera, 8.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor """,
        'price': 760.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone Max M2 Dual SIM TD-LTE IN 32GB ZB632KL',
        'description': """
        2018 Dec, Smartphone, 76.28x158.41x7.7 mm, Android,
        Qualcomm Snapdragon 632 SDM632, 3.00 GiB RAM, 32.0 GB ROM,
        720x1520, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 13.0 MP camera,
        1.9 MP aux. camera, 8.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor """,
        'price': 560.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone Max Pro M2 Dual SIM TD-LTE IN ID 64GB ZB630KL',
        'description': """
        2018 Dec, Smartphone, 75.5x157.9x8.5 mm, Android,
        Qualcomm Snapdragon 660 SDM660, 4.00 GiB RAM, 64.0 GB ROM,
        1080x2280, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 12.2 MP camera,
        4.9 MP aux. camera, 13.0 MP 2nd camera, 5000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor """,
        'price': 460.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone Max Pro M2 Dual SIM TD-LTE IN ID 32GB ZB630KL',
        'description': """
        2018 Dec, Smartphone, 75.5x157.9x8.5 mm, Android,
        Qualcomm Snapdragon 660 SDM660, 3.00 GiB RAM, 32.0 GB ROM,
        1080x2280, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 12.2 MP camera,
        4.9 MP aux. camera, 13.0 MP 2nd camera, 5000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 360.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone Live L1 Dual SIM TD-LTE IN ZA551KL 16GB',
        'description': """
        2018 May, Smartphone, 71.77x147.26x8.15 mm, Android,
        Qualcomm Snapdragon 430 MSM8937, 2.00 GiB RAM, 16.0 GB ROM,
        720x1440, 5.5 inch, Color IPS TFT LCD display,
        Dual standby operation, GPS, 13.0 MP camera, 4.9 MP 2nd camera,
        3000 mAh battery, Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 250.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone Max M1 Dual SIM TD-LTE IN ZB556KL 32GB',
        'description': """
        2018 Oct, Smartphone, 70.9x147.3x8.7 mm, Android,
        Qualcomm Snapdragon 430 MSM8937, 3.00 GiB RAM, 32.0 GB ROM,
        720x1440, 5.4 inch, Color IPS TFT LCD display, Dual standby operation,
        GPS, 13.0 MP camera, 8.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 300.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone 5 2018 Dual SIM TD-LTE JP Version B ZE620KL',
        'description': """
        2018 Jul, Smartphone, 75.65x153x7.85 mm, Android,
        Qualcomm Snapdragon 636 SDM636, 6.00 GiB RAM, 64.0 GB ROM,
        1080x2246, 6.2 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 12.2 MP camera,
        8.0 MP aux. camera, 8.0 MP 2nd camera, 3300 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor """,
        'price': 400.00,
        'stock': 20,
    },
    {
        'name': 'Asus ZenFone 5 2018 Dual SIM TD-LTE BR Version B ZE620KL 128GB',
        'description': """
        2018 Apr, Smartphone, 75.65x153x7.85 mm, Android,
        Qualcomm Snapdragon 636 SDM636, 4.00 GiB RAM, 128.0 GB ROM,
        1080x2246, 6.2 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 12.2 MP camera,
        8.0 MP aux. camera, 8.0 MP 2nd camera, 3300 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor """,
        'price': 880.00,
        'stock': 20,
    },

    # Apple
    {
        'name': 'Apple Watch Series 4 Nike+ 44mm TD-LTE NA',
        'description': """
        2018 Sep, Smartwatch, 38x44x10.7 mm, iOS, Apple S4, 768 MiB RAM,
        16.0 GB ROM, 368x448, 2 inch, AM-OLED display, NFC: Yes, GPS,
        292 mAh battery, Light sensor, Barometer, Altimeter, Heart rate sensor""",
        'price': 230.00,
        'stock': 20,
    },
    {
        'name': 'Apple Watch Series 4 Nike+ 40mm TD-LTE NA',
        'description': """
        2018 Oct, Smartwatch, 34x40x10.7 mm, iOS, Apple S4, 768 MiB RAM,
        16.0 GB ROM, 324x394, 1.8 inch, AM-OLED display, NFC: Yes, GPS,
        226 mAh battery, Light sensor, Barometer, Altimeter, Heart rate sensor """,
        'price': 250.00,
        'stock': 20,
    },
    {
        'name': 'Apple Watch Series 4 Nike+ 44mm A1978',
        'description': """
        2018 Sep, Smartwatch, 38x44x10.7 mm, iOS, Apple S4, 768 MiB RAM,
        16.0 GB ROM, 368x448, 2 inch, AM-OLED display, NFC: Yes, GPS,
        Light sensor, Barometer, Altimeter, Heart rate sensor""",
        'price': 230.00,
        'stock': 20,
    },
    {
        'name': 'Apple iPad Mini 5th gen 2019 TD-LTE CN A2125 64GB',
        'description': """
        NEW 2019 Mar, Tablet, 134.8x203.2x6.1 mm, iOS, Apple A12 Bionic
        APL1081 / APL1W81 (T8020), 3.00 GiB RAM, 64.0 GB ROM, 1536x2048,
        7.9 inch, Color IPS TFT LCD display, NFC (A), NFC (B), GPS,
        8.0 MP camera, 7.1 MP 2nd camera, 5124 mAh battery, Light sensor,
        Barometer, Fingerprint sensor""",
        'price': 330.00,
        'stock': 20,
    },
    {
        'name': 'Apple iPad Mini 5th gen 2019 TD-LTE JP A2126 256GB',
        'description': """
        NEW 2019 Mar, Tablet, 134.8x203.2x6.1 mm, iOS, Apple A12 Bionic
        APL1081 / APL1W81 (T8020), 3.00 GiB RAM, 256.0 GB ROM, 1536x2048,
        7.9 inch, Color IPS TFT LCD display, NFC (A), NFC (B), GPS,
        8.0 MP camera, 7.1 MP 2nd camera, 5124 mAh battery, Light sensor,
        Barometer, Fingerprint sensor""",
        'price': 360.00,
        'stock': 20,
    },
    {
        'name': 'Apple iPad Air 3rd gen 2019 Global TD-LTE A2123 256GB',
        'description': """
        NEW 2019 Mar, Tablet, 174.1x250.6x6.1 mm, iOS, Apple A12 Bionic
        APL1081 / APL1W81 (T8020), 3.00 GiB RAM, 256.0 GB ROM, 1668x2224,
        10.5 inch, Color IPS TFT LCD display, NFC (A), NFC (B), GPS,
        8.0 MP camera, 7.1 MP 2nd camera, 8050 mAh battery, Light sensor,
        Barometer, Fingerprint sensor""",
        'price': 430.00,
        'stock': 20,
    },
    {
        'name': 'Apple iPhone Xs A2099 Dual SIM TD-LTE CN 64GB',
        'description': """
        2018 Nov, Smartphone, 70.9x143.6x7.7 mm, iOS, Apple A12 Bionic
        APL1081 / APL1W81 (T8020), 4.00 GiB RAM, 64.0 GB ROM, 1125x2436,
        5.8 inch, 1 notch, AM-OLED display, Dual standby operation,
        NFC (A), NFC (B), GPS, 12.2 MP camera, 12.2 MP aux. camera,
        7.2 MP 2nd camera, 2659 mAh battery, Light sensor,
        Proximity sensor, Barometer, Infrared face sensor""",
        'price': 730.00,
        'stock': 20,
    },
    {
        'name': 'Apple iPhone XR A2108 Dual SIM TD-LTE CN 256GB',
        'description': """
        2018 Oct, Smartphone, 75.7x150.9x8.3 mm, iOS, Apple A12 Bionic
        APL1081 / APL1W81 (T8020), 3.00 GiB RAM, 256.0 GB ROM, 828x1792,
        6.1 inch, 1 notch, Color IPS TFT LCD display, Dual standby operation,
        NFC (A), NFC (B), GPS, 12.2 MP camera, 7.2 MP 2nd camera,
        2945 mAh battery, Light sensor, Proximity sensor, Barometer,
        Infrared face sensor""",
        'price': 900.00,
        'stock': 20,
    },

    # Huawei
    {
        'name': 'Huawei P30 Standard Edition Dual SIM TD-LTE CN ELE-TL00 128GB (Huawei Elle)',
        'description': """
        NEW 2019 Apr, Smartphone, 71.36x149.1x7.57 mm, Android,
        HiSilicon Honor KIRIN980, 6.00 GiB RAM, 128.0 GB ROM, 1080x2340,
        6.1 inch, 1 notch, AM-OLED display, Dual standby operation,
        NFC (A), NFC (B), GPS, 39.9 MP camera, 15.9 MP aux. camera,
        8.0 MP aux. 2 camera, 32.0 MP 2nd camera, 3650 mAh battery,
        Light sensor, Proximity sensor, Hall, Temperature sensor,
        In-screen fingerprint sensor""",
        'price': 1000.00,
        'stock': 20,
    },
    {
        'name': 'Huawei P30 Lite TD-LTE EMEA 128GB MAR-L01 (Huawei Marie Claire)',
        'description': """
        NEW 2019 May, Smartphone, 72.7x152.9x7.4 mm, Android,
        HiSilicon Honor KIRIN710, 4.00 GiB RAM, 128.0 GB ROM,
        1080x2312, 6.1 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 23.8 MP camera,
        8.0 MP aux. camera, 1.9 MP aux. 2 camera, 32.0 MP 2nd camera,
        3340 mAh battery, Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 750.00,
        'stock': 20,
    },
    {
        'name': 'Huawei Nova 4e Premium Edition Dual SIM TD-LTE APAC 128GB MAR-LX2',
        'description': """
        NEW 2019 Apr, Smartphone, 72.7x152.9x7.4 mm, Android,
        HiSilicon Honor KIRIN710, 6.00 GiB RAM, 128.0 GB ROM,
        1080x2312, 6.1 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, GPS, 23.8 MP camera, 8.0 MP aux. camera,
        1.9 MP aux. 2 camera, 32.0 MP 2nd camera, 3340 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 770.00,
        'stock': 20,
    },
    {
        'name': 'Huawei P30 Pro Premium Edition Dual SIM TD-LTE CN VOG-AL00 512GB (Huawei Vogue)',
        'description': """
        NEW 2019 Apr, Smartphone, 73.4x158x8.41 mm, Android,
        HiSilicon Honor KIRIN980, 8.00 GiB RAM, 512.0 GB ROM,
        1080x2340, 6.5 inch, 1 notch, AM-OLED display,
        Dual standby operation, NFC (A), NFC (B), IR: Yes,
        GPS, 39.9 MP camera, 19.7 MP aux. camera, 8.0 MP aux. 2 camera,
        32.0 MP 2nd camera, 4200 mAh battery, Light sensor, Proximity sensor,
        Hall, Temperature sensor, In-screen fingerprint sensor""",
        'price': 1200.00,
        'stock': 20,
    },
    {
        'name': 'Huawei Honor 20i Standard Edition Dual SIM TD-LTE CN 128GB HRY-AL00T (Huawei Henry-T)',
        'description': """
        NEW 2019 Apr, Smartphone, 73.64x154.8x7.95 mm, Android,
        HiSilicon Honor KIRIN710, 4.00 GiB RAM, 128.0 GB ROM, 1080x2340,
        6.2 inch, 1 notch, Color IPS TFT LCD display, Dual standby operation,
        GPS, 23.8 MP camera, 8.0 MP aux. camera, 1.9 MP aux. 2 camera,
        32.0 MP 2nd camera, 3400 mAh battery, Light sensor, Proximity sensor,
        Fingerprint sensor""",
        'price': 960.00,
        'stock': 20,
    },

    # Nokia
    {
        'name': 'Nokia 9 PureView Dual SIM TD-LTE CN',
        'description': """
        NEW 2019 Apr, Smartphone, 75x155x8 mm, Android,
        Qualcomm Snapdragon 845 SDM845 (Napali), 6.00 GiB RAM, 128.0 GB ROM,
        1440x2880, 6 inch, AM-OLED display, Dual standby operation,
        NFC (A), NFC (B), GPS, 12.2 MP camera, 12.2 MP aux. camera,
        12.2 MP aux. 2 camera, 12.2 MP aux. 3 camera, 12.2 MP aux. 4 camera,
        19.7 MP 2nd camera, 3320 mAh battery, Light sensor, Proximity sensor,
        Barometer, Altimeter, Hall, In-screen fingerprint sensor""",
        'price': 960.00,
        'stock': 20,
    },
    {
        'name': 'Nokia X71 Dual SIM TD-LTE APAC / 8.1 Plus',
        'description': """
        NEW 2019 Apr, Smartphone, 76.45x157.19x7.98 mm, Android,
        Qualcomm Snapdragon 660 SDM660, 6.00 GiB RAM, 128.0 GB ROM,
        1080x2316, 6.4 inch, 1 hole, Color IPS TFT LCD display,
        Dual standby operation, NFC (A), NFC (B), GPS, 64.0 MP camera,
        8.0 MP aux. camera, 4.9 MP aux. 2 camera, 15.9 MP 2nd camera,
        3500 mAh battery, Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 760.00,
        'stock': 20,
    },
    {
        'name': 'Nokia 9 PureView TD-LTE NA',
        'description': """
        NEW 2019 Mar, Smartphone, 75x155x8 mm, Android,
        Qualcomm Snapdragon 845 SDM845 (Napali), 6.00 GiB RAM,
        128.0 GB ROM, 1440x2880, 6 inch, AM-OLED display, NFC (A),
        NFC (B), GPS, 12.2 MP camera, 12.2 MP aux. camera,
        12.2 MP aux. 2 camera, 12.2 MP aux. 3 camera, 12.2 MP aux. 4 camera,
        19.7 MP 2nd camera, 3320 mAh battery, Light sensor, Proximity sensor,
        Barometer, Altimeter, Hall, In-screen fingerprint sensor""",
        'price': 160.00,
        'stock': 20,
    },
    {
        'name': 'Nokia 9 PureView Global Dual SIM TD-LTE',
        'description': """
        NEW 2019 Mar, Smartphone, 75x155x8 mm, Android,
        Qualcomm Snapdragon 845 SDM845 (Napali), 6.00 GiB RAM, 128.0 GB ROM,
        1440x2880, 6 inch, AM-OLED display, Dual standby operation, NFC (A),
        NFC (B), GPS, 12.2 MP camera, 12.2 MP aux. camera,
        12.2 MP aux. 2 camera, 12.2 MP aux. 3 camera, 12.2 MP aux. 4 camera,
        19.7 MP 2nd camera, 3320 mAh battery, Light sensor, Proximity sensor,
        Barometer, Altimeter, Hall, In-screen fingerprint sensor""",
        'price': 260.00,
        'stock': 20,
    },
    {
        'name': 'Nokia 6.1 Plus 2018 Premium Edition SIM TD-LTE IN (HMD DRG)',
        'description': """
        NEW 2019 Feb, Smartphone, 70.98x147.2x7.99 mm, Android,
        Qualcomm Snapdragon 636 SDM636, 6.00 GiB RAM, 64.0 GB ROM,
        1080x2280, 5.8 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, GPS, 15.9 MP camera, 4.9 MP aux. camera,
        15.9 MP 2nd camera, 3060 mAh battery, Light sensor, Proximity sensor,
        Fingerprint sensor""",
        'price': 360.00,
        'stock': 20,
    },
    {
        'name': 'Nokia 3.1 Plus Global TD-LTE 32GB (HMD 3.1 Plus)',
        'description': """
        2018 Dec, Smartphone, 76.44x156.88x8.19 mm, Android,
        MediaTek MT6762 (Helio P22), 3.00 GiB RAM, 32.0 GB ROM,
        720x1440, 6 inch, Color IPS TFT LCD display, NFC (A), NFC (B),
        GPS, 13.0 MP camera, 4.9 MP aux. camera, 8.0 MP 2nd camera,
        3500 mAh battery, Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 390.00,
        'stock': 20,
    },

    # Xiaomi
    {
        'name': 'Xiaomi Black Shark 2 Global Dual SIM TD-LTE 128GB SKW-H0 (Xiaomi Skywalker)',
        'description': """
        NEW 2019 Mar, Smartphone, 75.01x163.61x9.57 mm, Android,
        Qualcomm Snapdragon 855 SM8150 (Hana), 8.00 GiB RAM, 128.0 GB ROM,
        1080x2340, 6.4 inch, AM-OLED display, Dual standby operation,
        GPS, 48.0 MP camera, 12.2 MP aux. camera, 19.9 MP 2nd camera,
        4000 mAh battery, Light sensor, Proximity sensor, Barometer,
        Hall, In-screen fingerprint sensor""",
        'price': 1060.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Mi 9 SE Global Dual SIM TD-LTE M1903F2G 64GB (Xiaomi Grus)',
        'description': """
        NEW 2019 Apr, Smartphone, 70.5x147.5x7.45 mm, Android,
        Qualcomm Snapdragon 712 SDM712, 6.00 GiB RAM, 64.0 GB ROM,
        1080x2340, 6 inch, 1 notch, AM-OLED display, Dual standby operation,
        NFC (A), NFC (B), IR: Yes, GPS, 48.0 MP camera, 13.0 MP aux. camera,
        8.0 MP aux. 2 camera, 19.9 MP 2nd camera, 3070 mAh battery,
        Light sensor, Proximity sensor, In-screen fingerprint sensor """,
        'price': 1300.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Redmi 7 Standard Edition Dual SIM TD-LTE IN 32GB M1810F6LI (Xiaomi onclite)',
        'description': """
        NEW 2019 Apr, Smartphone, 75.58x158.73x8.47 mm, Android,
        Qualcomm Snapdragon 632 SDM632, 2.00 GiB RAM, 32.0 GB ROM,
        1520x720, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, IR: Yes, GPS, 12.2 MP camera,
        1.9 MP aux. camera, 8.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 260.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Redmi Y3 Standard Edition Dual SIM TD-LTE IN 32GB M1810F6I (Xiaomi onc)',
        'description': """
        NEW 2019 Apr, Smartphone, 75.58x158.73x8.47 mm, Android,
        Qualcomm Snapdragon 632 SDM632, 2.00 GiB RAM, 32.0 GB ROM,
        1520x720, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, IR: Yes, GPS, 12.2 MP camera,
        1.9 MP aux. camera, 32.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 330.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Redmi 7 Global Dual SIM TD-LTE 64GB M1810F6LG (Xiaomi onclite)',
        'description': """
        NEW 2019 Mar, Smartphone, 75.58x158.73x8.47 mm, Android,
        Qualcomm Snapdragon 632 SDM632, 3.00 GiB RAM, 64.0 GB ROM,
        1520x720, 6.3 inch, 1 notch, Color IPS TFT LCD display,
        Dual standby operation, IR: Yes, GPS, 12.2 MP camera,
        1.9 MP aux. camera, 8.0 MP 2nd camera, 4000 mAh battery,
        Light sensor, Proximity sensor, Fingerprint sensor""",
        'price': 570.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Black Shark 2 Dual SIM TD-LTE CN 256GB SKW-A0 (Xiaomi Skywalker)',
        'description': """
        NEW 2019 Apr, Smartphone, 75.01x163.61x9.57 mm, Android,
        Qualcomm Snapdragon 855 SM8150 (Hana), 12.00 GiB RAM, 25.6 GB ROM,
        1080x2340, 6.4 inch, AM-OLED display, Dual standby operation, GPS,
        48.0 MP camera, 12.2 MP aux. camera, 19.9 MP 2nd camera,
        4000 mAh battery, Light sensor, Proximity sensor, Barometer,
        Hall, In-screen fingerprint sensor""",
        'price': 360.00,
        'stock': 20,
    },
    {
        'name': 'Xiaomi Mi 9 SE Global Dual SIM TD-LTE M1903F2G 128GB/Mi 9 Lite (Xiaomi Grus)',
        'description': """
        NEW 2019 Apr, Smartphone, 70.5x147.5x7.45 mm, Android,
        Qualcomm Snapdragon 712 SDM712, 6.00 GiB RAM, 128.0 GB ROM,
        1080x2340, 6 inch, 1 notch, AM-OLED display, Dual standby operation,
        NFC (A), NFC (B), IR: Yes, GPS, 48.0 MP camera, 13.0 MP aux. camera,
        8.0 MP aux. 2 camera, 19.9 MP 2nd camera, 3070 mAh battery,
        Light sensor, Proximity sensor, In-screen fingerprint sensor""",
        'price': 1220.00,
        'stock': 20,
    }

]
